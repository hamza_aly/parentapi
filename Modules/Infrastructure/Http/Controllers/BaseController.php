<?php

namespace Modules\Infrastructure\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

abstract class BaseController extends Controller
{
    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function ok($data)
    {
        return response()->json($data,Response::HTTP_OK);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function accepted($data)
    {
        return response()->json($data,Response::HTTP_ACCEPTED);
    }
    
    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function created($data)
    {
        return response()->json($data,Response::HTTP_CREATED);
    }
}
