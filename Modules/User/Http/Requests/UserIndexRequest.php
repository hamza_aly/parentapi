<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserIndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'nullable|string|in:DataProviderX,DataProviderY',
            'statusCode' => 'nullable|string|in:authorised,decline,refunded',
            'balanceMin' => 'numeric|lt:balanceMax',
            'balanceMax' => 'numeric|gt:balanceMin',
            'currency' => 'string'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
