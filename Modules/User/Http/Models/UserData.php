<?php
namespace Modules\User\Http\Models;

class UserData
{
    /**
     * variables
     */
    private $balance;
    private $currency;
    private $email;
    private $status;
    private $created_at;
    private $id;

    /**
     * set balance
     *
     * @param string $balance
     * @return self
     */
    public function setBalance(string $balance): self 
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * get balance
     *
     * @return string
     */
    public function getBalance(): string 
    {
        return $this->balance;
    }

     /**
     * set currency
     *
     * @param string $currency
     * @return self
     */
    public function setCurrency(string $currency): self 
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * get currency
     *
     * @return string
     */
    public function getCurrency(): string 
    {
        return $this->currency;
    }

     /**
     * set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self 
    {
        $this->email = $email;
        return $this;
    }

    /**
     * get email
     *
     * @return string
     */
    public function getEmail(): string 
    {
        return $this->email;
    }


     /**
     * set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus(string $status): self 
    {
        $this->status = $status;
        return $this;
    }

    /**
     * get status
     *
     * @return string
     */
    public function getStatus(): string 
    {
        return $this->status;
    }


     /**
     * set created_at
     *
     * @param string $created_at
     * @return self
     */
    public function setCreatedAt(string $created_at): self 
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * get created_at
     *
     * @return string
     */
    public function getCreatedAt(): string 
    {
        return $this->created_at;
    }


     /**
     * set id
     *
     * @param string $id
     * @return self
     */
    public function setID(string $id): self 
    {
        $this->id = $id;
        return $this;
    }

    /**
     * get id
     *
     * @return string
     */
    public function getID(): string 
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}