<?php
namespace Modules\User\Http\Models\Interfaces;

use Modules\User\Http\Models\UserData;

interface UserDataInterface
{
    /**
     * Handel user data
     *
     * @param object $data
     * @param array $statusCode
     * @return UserData
     */
    public function handleUserData($data, array $statusCode): UserData;

     /**
     * convert status code from intiger to string.
     *
     * @param integer $status
     * @param array $statusCode
     * @return string
     */
    public function handleStatusCode(int $status, array $statusCode): string;

    /**
     * set data
     *
     * @param UserData $data
     * @return self
     */
    public function setData(UserData $data);

    /**
     * get data
     *
     * @return array
     */
    public function getData(): array;
}
