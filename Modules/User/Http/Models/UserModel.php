<?php

namespace Modules\User\Http\Models;

use Modules\User\Http\Models\Interfaces\UserDataInterface;

class UserModel
{
    /**
     * variables
     */
    private $data;
    private $rst;


    /**
     * set data
     *
     * @param UserDataInterface $provider
     * @return self
     */
    public function setData(UserDataInterface $provider): self
    {
        foreach ($provider->getData() as $row) {
            $this->data[] = $row;
        }
        return $this;
    }

    /**
     * get data
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * min
     *
     * @param string $key
     * @param integer $value
     * @return self
     */
    public function min(string $key, int $value): self
    {
        $data =  (count($this->rst) > 0) ? $this->rst : $this->data;
        $key = 'get' . ucfirst($key);
        $rst = [];

        foreach ($data as $row)
            if ($row->$key() >= $value)
                $rst[] = $row;

        $this->rst = $rst;
        return $this;
    }

    /**
     * max
     *
     * @param string $key
     * @param integer $value
     * @return self
     */
    public function max(string $key, int $value): self
    {
        $data =  (count($this->rst) > 0) ? $this->rst : $this->data;
        $key = 'get' . ucfirst($key);
        $rst = [];

        foreach ($data as $row)
            if ($row->$key() <= $value)
                $rst[] = $row;

        $this->rst = $rst;
        return $this;
    }

    /**
     * where
     *
     * @param string $key
     * @param string $value
     * @return self
     */
    public function where(string $key, string $value): self
    {
        $data =  (count($this->rst) > 0) ? $this->rst : $this->data;
        $key = 'get' . ucfirst($key);
        $rst = [];

        foreach ($data as $row)
            if ($row->$key() == $value)
                $rst[] = $row;

        $this->rst = $rst;
        return $this;
    }

    /**
     * get data
     *
     * @return array
     */
    public function get(): array
    {
        $data = (count($this->rst) > 0) ? $this->rst : $this->data;
        $array = [];
        foreach($data as $user){
            $array[] = $user->toArray();
        }
        return $array;
    }
}
