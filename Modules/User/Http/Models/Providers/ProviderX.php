<?php

namespace Modules\User\Http\Models\Providers;

use Modules\User\Http\Models\Interfaces\UserDataInterface;
use Modules\User\Http\Models\UserData;

class ProviderX implements UserDataInterface
{
    private $data;

    /**
     * construct function
     *
     * @param array $statusCode
     */
    public function __construct(array $statusCode)
    {
        $data = json_decode(file_get_contents(base_path(config('user.DATA_PROVIDER_X_PATH'))), false);
        foreach ($data as $row) {
            $this->setData($this->handleUserData($row, $statusCode));
        }
    }

    /**
     * Handel user data
     *
     * @param object $data
     * @param array $statusCode
     * @return UserData
     */
    public function handleUserData($data, array $statusCode): UserData
    {
        return (new UserData)
            ->setBalance($data->parentAmount)
            ->setCurrency($data->Currency)
            ->setEmail($data->parentEmail)
            ->setStatus($this->handleStatusCode($data->statusCode, $statusCode))
            ->setCreatedAt($data->registrationDate)
            ->setID($data->parentIdentification);
    }

    /**
     * set data
     *
     * @param UserData $data
     * @return self
     */
    public function setData(UserData $data)
    {
        $this->data[] = $data;
        return $this;
    }

    /**
     * get data
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * convert status code from intiger to string.
     *
     * @param integer $status
     * @param array $statusCode
     * @return string
     */
    public function handleStatusCode(int $status, array $statusCode): string
    {
        foreach ($statusCode as $key => $value)
            if (array_search($status, $value) !== false)
                return $key;

        return "";
    }
}
