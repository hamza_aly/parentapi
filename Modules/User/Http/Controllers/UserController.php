<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Infrastructure\Http\Controllers\BaseController;
use Modules\User\Http\Requests\UserIndexRequest;
use Modules\User\Services\Interfaces\UserServiceInterface;

class UserController extends BaseController
{

    private $userService;

    public function __construct(UserServiceInterface $referralService)
    {
        $this->userService = $referralService;
    }

    /**
     * Display a listing of the resource.
     * @param UserIndexRequest $request
     * @return Response
     */
    public function index(UserIndexRequest $request)
    {
        return $this->userService->getUsers($request->all());
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
