<?php

namespace Modules\User\Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_index_users_will_return_list_users()
    {
        $response = $this->json('GET', '/api/v1/users');
        $response->assertOk()
            ->assertJsonStructure([
                    '*' => [
                        'balance',
                        'currency',
                        'email',
                        'status',
                        'created_at',
                        'id'
                    ]
            ]);
    }

    public function test_index_users_will_return_422_status_code_when_pass_invalid_status_code_value()
    {
        $response = $this->json('GET', '/api/v1/users?statusCode=unauthorised');
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'statusCode' => ['The selected status code is invalid.'],
                ],
            ]);
    }

    public function test_index_users_will_return_422_status_code_when_pass_invalid_balance_minimum_and_maximum_values()
    {
        $response = $this->json('GET', '/api/v1/users?balanceMin=300&balanceMax=200');
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'balanceMin' => ['The balance min must be less than 200.'],
                    'balanceMax' => ['The balance max must be greater than 300.'],
                ],
            ]);
    }
}
