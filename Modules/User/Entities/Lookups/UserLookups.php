<?php


namespace Modules\User\Entities\Lookups;


class UserLookups
{
    const PROVIDER_X_AUTHORISED = 1;
    const PROVIDER_X_DECLINE = 2;
    const PROVIDER_X_REFUNDED = 3;

    const PROVIDER_Y_AUTHORISED = 100;
    const PROVIDER_Y_DECLINE = 200;
    const PROVIDER_Y_REFUNDED = 300;
}
