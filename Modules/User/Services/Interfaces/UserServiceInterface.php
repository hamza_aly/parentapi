<?php


namespace Modules\User\Services\Interfaces;


interface UserServiceInterface
{
    public function getUsers(array $array): array;

    public function filter(array $filters, array $statusCode): array;
}
