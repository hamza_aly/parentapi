<?php


namespace Modules\User\Services;


use Modules\User\Entities\Lookups\UserLookups;
use Modules\User\Http\Models\Providers\ProviderX;
use Modules\User\Http\Models\Providers\ProviderY;
use Modules\User\Http\Models\UserModel;
use Modules\User\Services\Interfaces\UserServiceInterface;

class UserService implements UserServiceInterface
{

    /**
     * Get users function
     *
     * @param array $array
     * @return array
     */
    public function getUsers(array $array): array
    {
        $statusCode = [
            'authorised' => [UserLookups::PROVIDER_X_AUTHORISED, UserLookups::PROVIDER_Y_AUTHORISED],
            'decline' => [UserLookups::PROVIDER_X_DECLINE, UserLookups::PROVIDER_Y_DECLINE],
            'refunded' => [UserLookups::PROVIDER_X_REFUNDED, UserLookups::PROVIDER_Y_REFUNDED]
        ];
        return $this->filter($array, $statusCode);
    }

    /**
     * filtter
     *
     * @param array $filters
     * @param array $statusCode
     * @return array
     */
    public function filter(array $filters, array $statusCode): array
    {
        $users = new UserModel;
        if (isset($filters['provider']) && !is_null($filters['provider'])) {
            $className = "Modules\User\Http\Models\Providers\\" . str_replace('Data', '', $filters['provider']);
            $users->setData((new $className($statusCode)));
        } else {
            $users->setData((new ProviderX($statusCode)));
            $users->setData((new ProviderY($statusCode)));
        }

        if (isset($filters['statusCode']) && !is_null($filters['statusCode']))
            $users->where('status', $filters['statusCode']);

        if (isset($filters['currency']) && !is_null($filters['currency']))
            $users->where('currency', $filters['currency']);

        if (isset($filters['balanceMin']) && !is_null($filters['balanceMin']))
            $users->min('balance', $filters['balanceMin']);

        if (isset($filters['balanceMax']) && !is_null($filters['balanceMax']))
            $users->max('balance', $filters['balanceMax']);

        return $users->get();
    }
}
