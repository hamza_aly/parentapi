<?php

return [
    'DATA_PROVIDER_X_PATH' => env("DATA_PROVIDER_X_PATH"),
    'DATA_PROVIDER_Y_PATH' => env("DATA_PROVIDER_Y_PATH"),
];
